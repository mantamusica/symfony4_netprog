<?php
/**
 * Created by PhpStorm.
 * User: chema
 * Date: 12/07/2019
 * Time: 11:58
 */

namespace App\Utils\Interfaces;

interface UploaderInterface {

    public function upload($file);
    public function delete($path);

}
